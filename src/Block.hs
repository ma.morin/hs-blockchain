module Block where

import qualified Crypto.Hash
import qualified Data.Binary
import qualified Data.ByteString
import qualified Data.ByteString.Lazy
import qualified Data.Int
import qualified Data.Maybe

data Block = Block {
  hash         :: Maybe (Crypto.Hash.Digest Crypto.Hash.SHA256),
  previousHash :: Maybe (Crypto.Hash.Digest Crypto.Hash.SHA256),
  nonce        :: Data.Int.Int32,
  isRoot       :: Bool
  --data         ::
} deriving (Show, Eq)

calculateHash :: Block -> Crypto.Hash.Digest Crypto.Hash.SHA256
calculateHash block =
  Crypto.Hash.hash (head (Data.ByteString.Lazy.toChunks (Data.Binary.encode (nonce block))))

calculateHashString :: Data.ByteString.ByteString -> Crypto.Hash.Digest Crypto.Hash.SHA256
calculateHashString = Crypto.Hash.hash

hashBlock :: Block -> Block
hashBlock block = Block
  (Just (calculateHash block))
  (previousHash block)
  (nonce block)
  (isRoot block)

isValid :: Block -> Bool
isValid block =
  ((isRoot block) || (Data.Maybe.isJust $ previousHash block)) &&
  (Data.Maybe.isJust $ hash block) &&
  (Just (calculateHash block) == hash block)

createBlock :: Maybe (Crypto.Hash.Digest Crypto.Hash.SHA256) -> Data.Int.Int32 -> Block
createBlock previousHash nonce = hashBlock (Block Nothing previousHash nonce False)

createRootBlock :: Data.Int.Int32 -> Block
createRootBlock nonce = hashBlock (Block Nothing Nothing nonce True)
