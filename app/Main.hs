module Main where
import qualified Math

main :: IO ()
main = do
  putStrLn "Hello World!"
  putStrLn ("Adding 1 and 2 equals " ++ show (Math.add 1 2))
