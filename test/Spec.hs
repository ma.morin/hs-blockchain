import           Block
import           Math
import           Test.Hspec
--import           Test.QuickCheck

main :: IO ()
main = hspec $ do
  describe "Math.add" $ do
    it "returns the sum of two integers" $ do
      (Math.add 1 5) `shouldBe` 6

  describe "Block.createRootBlock" $ do
    it "returns a new and valid root block" $ do
      let block = Block.createRootBlock 100
      Block.isValid block `shouldBe` True

  describe "Block.createBlock" $ do
    it "returns a new and valid block if the previous hash is specified." $ do
      let rootBlock = Block.createRootBlock 100
      let validBlock = Block.createBlock (Block.hash rootBlock) 100
      Block.isValid validBlock `shouldBe` True

      let invalidBlock = Block.createBlock Nothing 100
      Block.isValid invalidBlock `shouldBe` False
